from django.contrib import admin

from app.models import *

admin.site.register(Whois)
admin.site.register(Country)
admin.site.register(Category)
class TargetAdmin(admin.ModelAdmin):
	prepopulated_fields={'slug':('target',)}
admin.site.register(Target)
admin.site.register(domain_dnsrecords_db)
admin.site.register(Host)
admin.site.register(Port)

admin.site.register(Email_harvest_db)
admin.site.register(Web_app_info_db)
admin.site.register(Virtual_Host_db)


admin.site.register(Virtual_Host)
admin.site.register(TargetsPingTrack)
admin.site.register(Phone_Number)
admin.site.register(Vulnerabilities)
admin.site.register(Employee)
# admin.site.register(server_info)
admin.site.register(Service_provider)