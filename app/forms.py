from django import forms
from django.core.validators import MinValueValidator, MaxValueValidator


from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from datetime import date
from django.contrib.auth.models import User
from app.models import *

from django.forms import BaseModelFormSet


#DateInput = partial(forms.DateInput, {'class': 'datepicker'})
from functools import partial





class SignupForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = False
        

    def signup(self, request, user):
        user.email =self.cleaned_data['email']
        user.username=self.cleaned_data['email']

        user.save()
        

class TargetForm(forms.ModelForm):
    class Meta:
        model = Target
        exclude = ('slug','ip','status',)