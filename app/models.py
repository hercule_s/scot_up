from djongo import models
from django.template.defaultfilters import slugify
from django.utils.http import is_safe_url
from django.db.models.signals import post_save
from django.dispatch import receiver


class Country(models.Model):
    country = models.CharField(max_length=200)
    def __str__(self):
        return self.country

class Category(models.Model):
    category = models.CharField(max_length=200)
    def __str__(self):
        return self.category
    class Meta:
        verbose_name = "Data Source"
        verbose_name_plural ="Data Sources"

class Target(models.Model):
    name = models.CharField(max_length = 300)
    target = models.CharField(max_length = 300, help_text="example.com")
    country     = models.ForeignKey(Country, on_delete=models.CASCADE, null=True,blank=True)
    category    = models.ForeignKey(Category, on_delete = models.CASCADE, null=True, blank=True, verbose_name='Data Source')
    
    ip = models.CharField(max_length = 300)
   
    # domain = models.CharField(max_length = 300, verbose_name ="Target Domain")
    status = models.CharField(max_length = 300)
    slug=models.SlugField(unique=True, null=True,blank=True)
    def save(self,*args,**kwargs):
        self.slug=slugify(self.target)
        super(Target, self).save(*args,**kwargs)

    def __str__(self):
        return self.target

# @receiver(post_save, sender=Target, dispatch_uid="create_target_status_tracking_instance")
# def post_save(sender, instance, **kwargs):
#      instance.targetspingtrack = instance
#      instance.targetspingtrack.ip = instance.ip
#      instance.targetspingtrack.status = instance.status
#      instance.targetspingtrack.save()



class Employee(models.Model):
    company = models.ForeignKey(Target,on_delete=models.CASCADE, verbose_name="Target Company")
    name = models.CharField(max_length=300)
    linked_url = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.name

class TargetsPingTrack(models.Model):
    target = models.ForeignKey(Target, on_delete=models.CASCADE)
    ip = models.CharField(max_length=200)
    status = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now_add=True)
    def  __str__(self):
        return self.target.target

class Whois(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    ip = models.CharField(max_length=500)
    sh_domain =  models.CharField(max_length=500)
    city =  models.CharField(max_length=500)
    country =  models.CharField(max_length=500)
    description =  models.CharField(max_length=500)

    asn = models.CharField(max_length=500)
    asn_cidr = models.CharField(max_length=500)
    asn_registry = models.CharField(max_length=500)
    status =  models.CharField(max_length=500)


    emails =  models.CharField(max_length=500)
    name =  models.CharField(max_length=500)
    ip_range =  models.CharField(max_length=500)
    state = models.CharField(max_length=500)
    server_info = models.CharField(max_length=500)
    registrar = models.CharField(max_length=500)
    owner = models.CharField(max_length=500)
    owner_city = models.CharField(max_length=500)
    owner_state = models.CharField(max_length=500)
    owner_country =models.CharField(max_length=500)
    owner_company =models.CharField(max_length=500)
    domain_emails = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target

    

class domain_dnsrecords_db(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    soa_records =   models.CharField(max_length=500)
    mx_records =    models.CharField(max_length=500)
    txt_records =   models.CharField(max_length=500)
    a_records =     models.CharField(max_length=500)
    name_server_records =  models.CharField(max_length=500)
    cname_records = models.CharField(max_length=500)
    aaaa_records =  models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target

class Service_provider(models.Model):
    isp = models.CharField(max_length=500)
    code = models.CharField(max_length=500)
    ip_adresses = models.CharField(max_length=500)
    def __str__(self):
        return self.isp
    
class Host(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    host = models.CharField(max_length=500)
    status = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target

    

class Port(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    port = models.CharField(max_length=500)
    status = models.CharField(max_length=500)
    name = models.CharField(max_length=500)
    product = models.CharField(max_length=500)
    version = models.CharField(max_length=500)
    server_extrainfo = models.CharField(max_length=500)
    cpe = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target

  

class Email_harvest_db(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    email_records = models.CharField(max_length=500)
    employee = models.ForeignKey(Employee,on_delete=models.CASCADE, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target


class Web_app_info_db(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    key = models.CharField(max_length=500)
    app_info = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target
  

class Virtual_Host_db(models.Model):
    ip = models.CharField(max_length=500)
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    main_domain = models.CharField(max_length=500)
    location = models.CharField(max_length=500)
    routedesc = models.CharField(max_length=500)
    asname = models.CharField(max_length=500)
    bgp_route = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target

class Virtual_Host(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    v_hosts = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target
    
    
class Web_search(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    title = models.CharField(max_length=500)
    desc = models.CharField(max_length=500)
    url = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target


class Pagelinks(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    links = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target


class Vulnerabilities(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    exploit =  models.CharField(max_length=500)
    web_vuln_tech = models.CharField(max_length=500)
    web_vuln_name = models.CharField(max_length=500)
    web_vuln_description = models.CharField(max_length=500)
    web_vuln_severity_class = models.CharField(max_length=500)
    web_vuln_ref = models.CharField(max_length=500)

class SysVulnerabilities(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    service_vuln_tech = models.CharField(max_length=500)
    vulnerability = models.CharField(max_length=500)
    vuln_description = models.CharField(max_length=500)
    severity = models.CharField(max_length=500)
    exploit =  models.CharField(max_length=500)
    cvss_score = models.CharField(max_length=500)

class Phone_Number(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    employee = models.ForeignKey(Employee,on_delete=models.CASCADE, null=True, blank=True)
    phone_number = models.CharField(max_length=500)
    
# class server_info(models.Model):
#     target = models.ForeignKey(Target,on_delete=models.CASCADE, default=1)
#     os_name = models.CharField(max_length=500)
#     os_vendor = models.CharField(max_length=500)
#     os_gen = models.CharField(max_length=500)


class WhoisStaging(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    ip = models.CharField(max_length=500)
    sh_domain =  models.CharField(max_length=500)
    city =  models.CharField(max_length=500)
    country =  models.CharField(max_length=500)
    description =  models.CharField(max_length=500)

    asn = models.CharField(max_length=500)
    asn_cidr = models.CharField(max_length=500)
    asn_registry = models.CharField(max_length=500)
    status =  models.CharField(max_length=500)


    emails =  models.CharField(max_length=500)
    name =  models.CharField(max_length=500)
    ip_range =  models.CharField(max_length=500)
    state = models.CharField(max_length=500)
    server_info = models.CharField(max_length=500)
    registrar = models.CharField(max_length=500)
    owner = models.CharField(max_length=500)
    owner_city = models.CharField(max_length=500)
    owner_state = models.CharField(max_length=500)
    owner_country =models.CharField(max_length=500)
    owner_company =models.CharField(max_length=500)
    domain_emails = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target

    

class domain_dnsrecords_db_staging(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    soa_records =   models.CharField(max_length=500)
    mx_records =    models.CharField(max_length=500)
    txt_records =   models.CharField(max_length=500)
    a_records =     models.CharField(max_length=500)
    name_server_records =  models.CharField(max_length=500)
    cname_records = models.CharField(max_length=500)
    aaaa_records =  models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target

    
class HostStaging(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    host = models.CharField(max_length=500)
    status = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target

    

class PortStaging(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    port = models.CharField(max_length=500)
    status = models.CharField(max_length=500)
    name = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target

  

class Email_harvest_db_staging(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    email_records = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target


class Web_app_info_db_staging(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    key = models.CharField(max_length=500)
    app_info = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target
  

class Virtual_Host_db_staging(models.Model):
    ip = models.CharField(max_length=500)
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    main_domain = models.CharField(max_length=500)
    location = models.CharField(max_length=500)
    routedesc = models.CharField(max_length=500)
    asname = models.CharField(max_length=500)
    bgp_route = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target

class Virtual_Host_Staging(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    v_hosts = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target
    
    

class PagelinksStaging(models.Model):
    target = models.ForeignKey(Target,on_delete=models.CASCADE)
    links = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.target.target

class Notification(models.Model):
    notification = models.CharField(max_length=500)
    viewed = models.BooleanField(default=False)
    