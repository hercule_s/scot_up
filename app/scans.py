import socket
from app.models import *
from ipwhois import IPWhois
import nmap
from wig.wig import wig as WG
from botanick import Botanick as bt
import dns.resolver
import subprocess
import shlex
import robtex_python as rob
from domain import domain_dnsrecords
from extract_emails import ExtractEmails
import urllib.parse
import urllib
import urllib.request, re
import json
import pythonwhois
import whois
from bs4 import BeautifulSoup
import requests
from builtwith import builtwith
from pymongo import MongoClient
import getsploit.getsploit as sploit
import vulners
from zapv2 import ZAPv2
import pandas as pd
import os
from selenium import webdriver
from pyvirtualdisplay import Display
from linkedin_scraper import Person
import smtplib
from selenium import webdriver
from pyvirtualdisplay import Display
from time import sleep
import sys

def domaintoip(domain):
    return socket.gethostbyname(domain)

def domains_whois(search_text,ip_addr):
    target = Target.objects.get(target=search_text)
    data = IPWhois(ip_addr)
    out2 = data.lookup_rdap(depth=1)
    out = data.lookup_whois(get_referral=True)
    results = pythonwhois.get_whois(search_text)
    whois_results = whois.whois(search_text)

    asn = out2["asn"]
    asn_cidr = out2["asn_cidr"]
    asn_registry =out2["asn_registry"]
    status = out2["network"]["status"]



    city_data = out["nets"][0]['city']
    country_data = out["nets"][0]['country']
    description_data = out["nets"][0]['description']
    emails_data = out["nets"][0]['emails']
    name_data = out["nets"][0]['name']
    range_data = out["nets"][0]['range']
    state_data = out["nets"][0]['state']
    

    registrar_data = whois_results.registrar
    server_info = whois_results.whois_server
    domain_owner = whois_results.name
    owner_city = whois_results.city
    owner_state = whois_results.state
    owner_country = whois_results.country
    owner_company = whois_results.org
    domain_emails = whois_results.emails
    try:
        Whois.objects.get(target=target)
        WhoisStaging.objects.filter(target=target).delete()
        WhoisStaging.objects.create(target=target,ip=str(ip_addr), emails=str(emails_data),sh_domain=str(search_text),  city=str(city_data), country=str(country_data),
                     description=str(description_data),  name=str(name_data),
                     ip_range=str(range_data), registrar=registrar_data,server_info=server_info, 
                     owner=domain_owner,owner_city=owner_city, owner_state=owner_state,
                     owner_country=owner_country, owner_company=owner_company,
                     domain_emails=domain_emails, state=str(state_data),asn =str(asn),asn_cidr = str(asn_cidr),
                     asn_registry =str(asn_registry),status = str(status))
    except:
        Whois.objects.create(target=target,ip=str(ip_addr), emails=str(emails_data),sh_domain=str(search_text),  city=str(city_data), country=str(country_data),
                         description=str(description_data),  name=str(name_data),
                         ip_range=str(range_data), registrar=registrar_data,server_info=server_info, 
                         owner=domain_owner,owner_city=owner_city, owner_state=owner_state,
                         owner_country=owner_country, owner_company=owner_company,
                         domain_emails=domain_emails, state=str(state_data),asn =str(asn),asn_cidr = str(asn_cidr),
                         asn_registry =str(asn_registry),status = str(status))


def ports(search_text,ip_addr):
    target = Target.objects.get(target=search_text)
    nm = nmap.PortScanner()
    nm.scan(ip_addr, '21-100', arguments="-A -O -T4 -sV --version-intensity 5")

    for host in nm.all_hosts():
        status = nm[host].state()
        hs = str(host)
        try:
            Host.objects.get(target=target, host=hs)
            HostStaging.objects.filter(target=target, host=hs).delete()
            HostStaging.objects.create(target=target, host=hs, status=status)
        except:
            Host.objects.create(target=target, host=hs, status=status)
            Target.objects.filter(target=search_text).update(status=status)

        for proto in nm[host].all_protocols():
            lport = nm[host][proto].keys()

            for port in lport:
                name = nm[host][proto][port]['name']
                product = nm[host][proto][port]['product']
                version = nm[host][proto][port]['version']
                server_extrainfo = nm[host][proto][port]['extrainfo']
                cpe = nm[host][proto][port]['cpe']
                service_vuln_tech = product
                # vulnerabilities and exploits
                vulners_api = vulners.Vulners()
                try:
                    vulnerability_results = vulners_api.cpeVulnerabilities(cpe)
                    vulnerability = vulnerability_results['NVD'][0]['title']
                    vuln_description = vulnerability_results['NVD'][0]['description']
                except:
                    try:
                        vulnerability_results = vulners_api.softwareVulnerabilities(product, version)
                        vulnerability = vulnerability_results['software'][0]['cvss']
                        vuln_description = vulnerability_results['software'][0]['description']
                    except:
                        vulnerability = ''
                        vuln_description = ''
                exploit_results = sploit.exploitSearch(product)
                exploit = exploit_results[1]['search'][1]['_source']['title']
                severity = exploit_results[1]['search'][1]['_score']
                cvss_score = exploit_results[1]['search'][1]['_source']['cvss']['score']
                exploit_description = exploit_results[1]['search'][1]['_source']['description']

                if nm[host][proto][port]['state'] == 'open':
                    try:
                        Vulnerabilities.get(target=target)
                        Port.objects.get(target=target, port=str(port))
                        PortStaging.objects.filter(target=target, port=str(port)).delete()
                        PortStaging.objects.create(target=target, port=str(port),
                                                   status=str(nm[host][proto][port]['state']), name=str(name),
                                                   product=str(product), version=str(version),
                                                   server_extrainfo=str(server_extrainfo), cpe=str(cpe))
                    except:
                        SysVulnerabilities.objects.update_or_create(target=target, vulnerability=str(vulnerability),
                                                                 vuln_description=str(vuln_description),
                                                                 severity=str(severity), exploit=str(exploit), cvss_score=str(cvss_score), service_vuln_tech= str(service_vuln_tech))
                        Port.objects.update_or_create(target=target,
                                                      port=str(port), status=str(nm[host][proto][port]['state']),
                                                      name=str(name), product=str(product), version=str(version),
                                                      server_extrainfo=str(server_extrainfo), cpe=str(cpe))
                elif nm[host][proto][port]['state'] == 'filtered':
                    try:
                        Vulnerabilities.get(target=target)
                        Port.objects.get(target=target, port=str(port))
                        PortStaging.objects.filter(target=target, port=str(port)).delete()
                        PortStaging.objects.create(target=target, port=str(port),
                                                   status=str(nm[host][proto][port]['state']), name=str(name),
                                                   product=str(product), version=str(version),
                                                   server_extrainfo=str(server_extrainfo), cpe=str(cpe))
                    except:
                        SysVulnerabilities.objects.update_or_create(target=target, vulnerability=str(vulnerability),
                                                                 vuln_description=str(vuln_description),
                                                                 severity=str(severity), cvss_score=str(cvss_score),service_vuln_tech= str(service_vuln_tech))
                        Port.objects.update_or_create(target=target,
                                                      port=str(port), status=str(nm[host][proto][port]['state']),
                                                      name=str(name), product=str(product), version=str(version),
                                                      server_extrainfo=str(server_extrainfo), cpe=str(cpe))
                else:
                    pass

def email_harvest(search_text, engine,ip_addr):
    bt.callEmailHarvester(search_text, engine)
    email_records = bt.getResults()
    parsed = urllib.parse.urlparse(search_text)
    target = Target.objects.get(target=search_text)
    if parsed.scheme == '':
        try:
            replaced_parse = parsed._replace(scheme='https')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            extract_from_domain = ExtractEmails(url, depth=None, print_log=False, ssl_verify=True, user_agent='random')
            email_records2 = extract_from_domain.emails
            Email_harvest_db.objects.update_or_create( target=target,email_records=str(email_records),
                )
            
            for email in email_records2:
                try:
                    Email_harvest_db.objects.get(target=target,
                    email_records =str(email))
                    Email_harvest_db_staging.objects.filter(target=target,
                    email_records =str(email)).delete()
                    Email_harvest_db_staging.objects.create(target=target,
                    email_records =str(email))
                except:
                    Email_harvest_db.objects.update_or_create(target=target,
                    email_records =str(email))
        except:
            replaced_parse = parsed._replace(scheme='http')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            extract_from_domain = ExtractEmails(url, depth=None, print_log=False, ssl_verify=True, user_agent='random')
            email_records2 = extract_from_domain.emails
            Email_harvest_db.objects.update_or_create( target=target,email_records=str(email_records))
            
            for email in email_records2:
                try:
                    Email_harvest_db.objects.get(target=target,
                    email_records =str(email))
                    Email_harvest_db.objects_staging.update_or_create(target=target,
                    email_records =str(email))
                except:
                    Email_harvest_db.objects.update_or_create(target=target,
                    email_records =str(email))
            


def phone_numbers(search_text,ip_addr):
    parsed = urllib.parse.urlparse(search_text)
    target = Target.objects.get(target=search_text)
    if parsed.scheme == '':
        try:
            replaced_parse = parsed._replace(scheme='http')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            f = urllib.request.urlopen(url)
            s = f.read()
            phones=re.findall(b"(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})",s)
            for phone in phones:
                Phone_Number.objects.update_or_create(target=target, phone_number=phone.decode('utf-8'))
        except:
            replaced_parse = parsed._replace(scheme='https')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            f = urllib.request.urlopen(url)
            s = f.read()
            phones=re.findall(b"(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})",s)
            for phone in phones:
                Phone_Number.objects.update_or_create(target=target, phone_number=phone.decode('utf-8'))

def domain_dns_records(domain,ip_addr):
    target = Target.objects.get(target=domain)
    soa_records = domain_dnsrecords.fetch_dns_records(domain, 'SOA')
    soa_out = ("".join(map(str, soa_records)))
    mx_records = domain_dnsrecords.fetch_dns_records(domain, 'MX')
    mx_out = ("".join(map(str, mx_records)))
    txt_records = domain_dnsrecords.fetch_dns_records(domain, 'TXT')
    txt_out = ("".join(map(str, txt_records)))
    a_records = domain_dnsrecords.fetch_dns_records(domain, 'A')
    a_out = ("".join(map(str, a_records)))
    name_server_records = domain_dnsrecords.fetch_dns_records(domain, 'NS')
    name_out = ("".join(map(str, name_server_records)))
    cname_records = domain_dnsrecords.fetch_dns_records(domain, 'CNAME')
    cname_out = ("".join(map(str, cname_records)))
    aaaa_records = domain_dnsrecords.fetch_dns_records(domain, 'AAAA')
    print (aaaa_records)
    aaaa_out = ("".join(map(str, aaaa_records)))
    try:
        domain_dnsrecords_db.objects.get(target=target)
        domain_dnsrecords_db_staging.objects.update_or_create(target=target,soa_records=str(soa_out),
         aaaa_records=str(aaaa_out),cname_records=str(cname_out),
           a_records=str(a_out),txt_records=str(txt_out),mx_records=str(mx_out))
    except:
        domain_dnsrecords_db.objects.create(target=target,soa_records=str(soa_out),
         aaaa_records=str(aaaa_out),cname_records=str(cname_out),
           a_records=str(a_out),txt_records=str(txt_out),mx_records=str(mx_out))
   

def virtual_hosts(search_text,ip_addr):
    response = rob.ip_query(ip_addr)
    country = response['country']
    try:
        autonomous_system_name = response['asname']
    except:
        autonomous_system_name = "None"
    try:
        bgp_route = response['bgproute']
    except:
        bgp_route = "None"
    # try:
    target = Target.objects.get(target=search_text)
    passive_domains = response['pas']
    for d in passive_domains:
        v = d['o']
        try:
            Virtual_Host.objects.get(target=target,v_hosts=str(v))
            Virtual_Host_Staging.objects.update_or_create(target=target,v_hosts=str(v))
        except:
            Virtual_Host.objects.update_or_create(target=target,v_hosts=str(v))
  
    inactive_domains = response['pash']
    for d in inactive_domains:
        v = d['o']
    
        try:
            Virtual_Host.objects.get(target=target,v_hosts=str(v))
            Virtual_Host_Staging.objects.update_or_create(target=target,v_hosts=str(v))
        except:
            Virtual_Host.objects.update_or_create(target=target,v_hosts=str(v))
  
    try:
        main_domain = response['act']
        for d in main_domain:
            v = d['o']
            try:
                Virtual_Host_db.objects.get(target=target,main_domain=str(v),
                 location=str(country),asname=str(autonomous_system_name),
                  bgp_route=str(bgp_route))
                Virtual_Host_db_staging.objects.update_or_create(target=target,main_domain=str(v),
                 location=str(country),asname=str(autonomous_system_name),
                  bgp_route=str(bgp_route))
            except:
                Virtual_Host_db.objects.create(target=target,main_domain=str(v),
                 location=str(country),asname=str(autonomous_system_name),
                  bgp_route=str(bgp_route))
    except:
        try:
            Virtual_Host_db.objects.get(main_domain=search_text, target=target,
             location=str(country),asname=str(autonomous_system_name),
              bgp_route=str(bgp_route))
            Virtual_Host_db_staging.objects.update_or_create(main_domain=search_text, target=target,
             location=str(country),asname=str(autonomous_system_name),
              bgp_route=str(bgp_route))
        except:
            Virtual_Host_db.objects.create(main_domain=search_text, target=target,
             location=str(country),asname=str(autonomous_system_name),
              bgp_route=str(bgp_route))
               



def pagelinks_domain(domain, search_text):
    target = Target.objects.get(target=search_text)
    pagelinks_records = domain_pagelinks.pagelinks(domain)
    pagelinks_records = set(pagelinks_records)
    for x in pagelinks_records:
      
        save_data = domain_pagelinks_db.objects.update_or_create(domain_pagelink=x, target=target)


def web_search(search_text,ip_addr):
    # text = str(search_text + ":")
    # query = raw_input("search_text:")
    target = Target.objects.get(target=search_text)
    url = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&"
    query = urllib.parse.urlencode( {'q' : search_text } )
    response = urllib.request.urlopen (url + query ).read()
    data = json.loads ( response.decode() )
    results = data[ 'responseData' ][ 'results' ]
    for res in results:
        title=res.title.encode("utf8")
        desc=res.desc.encode("utf8")
        url =res.url.encode("utf8")
        Web_search.objects.create(target=target,
        desc=str(desc),url=str(url))


def pagelinks(domain,ip_addr):
    target = Target.objects.get(target=domain)
    parsed = urllib.parse.urlparse(domain)
    if parsed.scheme == '':
        try:
            replaced_parse = parsed._replace(scheme='http')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            soup = BeautifulSoup(requests.get(url).text, 'lxml')

            hrefs = []

            for a in soup.find_all('a'):
                hrefs.append(a['href'])

            for link in hrefs:
                try:
                    Pagelinks.objects.get(target=target,
                    links=link)
                    PagelinksStaging.objects.update_or_create(target=target,
                    links=link)
                except:
                    Pagelinks.objects.update_or_create(target=target,
                    links=link)



        except:
            replaced_parse = parsed._replace(scheme='https')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            soup = BeautifulSoup(requests.get(url).text, 'lxml')

            hrefs = []

            for a in soup.find_all('a'):
                hrefs.append(a['href'])

            for link in hrefs:
                try:
                    Pagelinks.objects.get(target=target,
                    links=link)
                    PagelinksStaging.objects.update_or_create(target=target,
                    links=link)
                except:
                    Pagelinks.objects.update_or_create(target=target,
                    links=link)



def web_app_info(search_text,ip_addr):
    target = Target.objects.get(target=search_text)
    parsed = urllib.parse.urlparse(search_text)
   
    if parsed.scheme == '':
        try:
            replaced_parse = parsed._replace(scheme='http')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            app_information = builtwith(url)
            for key, value in app_information.items():
                try:
                    Web_app_info_db.objects.get(target=target,
                    key=str(key), app_info="".join([str(x) for x in value] ))
                    Web_app_info_db_staging.objects.update_or_create(target=target,
                   key=str(key), app_info="".join([str(x) for x in value] ))
                except:
                    Web_app_info_db.objects.update_or_create(target=target,
                    key=str(key), app_info="".join([str(x) for x in value] ))

        except:
            replaced_parse = parsed._replace(scheme='https')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            app_information = builtwith(url)
            for key, value in app_information.items():
                try:
                    Web_app_info_db.objects.get(target=target,
                    key=str(key), app_info="".join([str(x) for x in value] ))
                    Web_app_info_db_staging.objects.update_or_create(target=target,
                   key=str(key), app_info="".join([str(x) for x in value] ))
                except:
                    Web_app_info_db.objects.update_or_create(target=target,
                    key=str(key), app_info="".join([str(x) for x in value] ))

            # Web_app_info_db.objects.filter(target=search_text, app_info=app_information).update_one(
            #     target=str(search_text),
            #     app_info=str(app_information), upsert=True)


def vulnerabilities(domain,ip_addr):
    target = Target.objects.get(target=domain)
    apikey = 'apikey'
    zap = ZAPv2(apikey=apikey)
    parsed = urllib.parse.urlparse(domain)
    if parsed.scheme == '':
        try:
            replaced_parse = parsed._replace(scheme='http')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            zap.urlopen(url)
            results = zap.core.alerts()
            for vuln in results:
                web_vuln_tech = vuln['param']
                web_vuln_name = vuln['name']
                web_vuln_description = vuln['description']
                web_vuln_severity_class = vuln['risk']
                web_vuln_ref = vuln['reference']

                Vulnerabilities.objects.update_or_create(target=target, web_vuln_tech=web_vuln_tech, web_vuln_name=web_vuln_name, web_vuln_description=web_vuln_description, web_vuln_severity_class=web_vuln_severity_class, web_vuln_ref=web_vuln_ref)
        except:
            replaced_parse = parsed._replace(scheme='https')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            zap.urlopen(url)
            results = zap.core.alerts()
            for vuln in results:
                web_vuln_tech = vuln['param']
                web_vuln_name = vuln['name']
                web_vuln_description = vuln['description']
                web_vuln_severity_class = vuln['risk']
                web_vuln_ref = vuln['reference']
                # try:
                #     Vulnerabilities.objects.get(target=target)
                # except:
                Vulnerabilities.objects.update_or_create(target=target, web_vuln_tech=web_vuln_tech, web_vuln_name=web_vuln_name, web_vuln_description=web_vuln_description, web_vuln_severity_class=web_vuln_severity_class, web_vuln_ref=web_vuln_ref)
    

def nikto_vuln(domain,ip_addr):
    target = Target.objects.get(target=domain)
    current_dir = os.getcwd()
    outfile = domain+".csv"
    os.chdir(current_dir+"/data_files")
    os.system("touch" + " " + outfile)
    os.system("nikto" + " " + "-h" + " " + domain + " " + "-o" + " " + outfile)
    df = pd.read_csv(outfile, skiprows=1)
    df.columns = ['target', 'ip', 'port', 'osvdb-id', 'tech/params', 'dir/files/misconfigs', 'vulnerability']
    results_dict = df.set_index('dir/files/misconfigs').T.to_dict('list')
    for d in results_dict.keys():
        individual_details = results_dict[d]
        files_dirs_misconfigs = d
        vulnerabilities = individual_details[5]
        params = individual_details[4]
        port = individual_details[2]
        osvdb = individual_details[3]
        SysVulnerabilities.objects.update_or_create(target=target, service_vuln_tech=port, vulnerability=vulnerabilities, vuln_description=files_dirs_misconfigs, cvss_score=osvdb)




# def linkedIn(domain_name):
#     target = Target.objects.get(target=domain_name)
#     domain =target.target
#     organization = target.name

#     current_dir = os.getcwd()
#     os.chdir(current_dir+"/LinkedIn")
#     os.system("python2" + " raven.py" + " -c" + "'" +organization + "'"+ " -s" + " ke" + " -d" + domain + " -p" + " 3")

    # #automate login
    # username = "kim34@tutanota.com"
    # password = "@#MkenyaDaima"

    # display = Display(visible=0, size=(1600, 1024))
    # display.start()
    # driver = webdriver.Firefox()
    # driver.delete_all_cookies()

    # driver.get("https://www.linkedin.com/uas/login")
    # driver.execute_script('localStorage.clear();')
    # # Fixed UTF-8 issue in title.

    # lnkUsername = driver.find_element_by_id("session_key-login")
    # lnkUsername.send_keys(username)
    # lnkPassword = driver.find_element_by_id("session_password-login")
    # lnkPassword.send_keys(password)
    # driver.find_element_by_id("btn-primary").click()

    # employee = Person("https://www.linkedin.com/in/ken-mwaura-56a63ab7", driver=driver)
    # print(employee)





def generate_emails(name, domain,employee_id):
    firstname =name.split()[0].lower()
    lastname =name.split()[1].lower()
    e1 = firstname + "@" + domain 
  
    #michaelombwayo@domain.com
    e2= firstname + lastname + "@" + domain 

    #michael.ombwayo@domain.com
    e3 = firstname + "." + lastname + "@" + domain

    #ombwayo@domain.com
    e4 = lastname + "@" + domain 

    #mombwayo@domain.com
    e5 = firstname[0] + lastname + "@" + domain 

    #m.ombwayo@domain.com
    e6= firstname[0] + "." + lastname + "@" + domain 

    #michaelo@domain.com
    e7 = firstname + lastname[0] + "@" + domain 

    #michael.o@domain.com
    e8 = firstname + "." + lastname[0] + "@" + domain 

    #mo@domain.com
    e9 = firstname[0] + lastname[0] + "@" + domain


  
    #michaelombwayo@domain.com
    e10= lastname + firstname + "@" + domain 

    #michael.ombwayo@domain.com
    e11 = lastname + "." + firstname + "@" + domain
    #mombwayo@domain.com
    e12 = lastname[0] + firstname + "@" + domain 

    #m.ombwayo@domain.com
    e13= lastname[0] + "." + firstname + "@" + domain 

    #michaelo@domain.com
    e14 = lastname + firstname[0] + "@" + domain 

    #michael.o@domain.com
    e15 = lastname + "." + firstname[0] + "@" + domain 

    #mo@domain.com
    e16 = lastname[0] + firstname[0] + "@" + domain

    emails =[e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12,e13,e14,e15,e16]
    target =Target.objects.get(target=domain)
    print("Generated Emails in relation to " +firstname +lastname)
    print("////////////////////////////")
    print("////////////////////////////")
    for email in emails:
        try:
            records = dns.resolver.query(domain, 'MX')
            mxRecord = records[0].exchange
            mxRecord = str(mxRecord)

            #check if the email address Exist
            
            # Get local server hostname
            host = socket.gethostname()

            # SMTP lib setup (use debug level for full output)
            server = smtplib.SMTP()
            server.set_debuglevel(0)

            # SMTP Conversation
            server.connect(mxRecord)

            # SMTP Conversation
            server.connect(mxRecord)
            server.helo(host)
            server.mail('me@domain.com')
            code, message = server.rcpt(str(email))
            server.quit()

            # Assume 250 as Success
            if code == 250:
                emp =Employee.objects.get(id=employee_id)
                Email_harvest_db.objects.update_or_create(employee=emp, email_records=email, target=target)
                print(email + " " +'Exists')
            else:
                print(email + " " +' Does Not Exist')
        except:
            print("Connection Refused")






def link_in(domain_name):
    target =Target.objects.get(target=domain_name)
    linkedinUsername = "kim34@keemail.me"
    linkedinPassword = "@#MkenyaDaima"
    company = "'" +target.name + "'"#"'ICT Authority'"
    state ="ke"
    domain = str(domain_name)
    pages =3
    class Requester(object):

        timeout = 10

        def __init__(self):
            display = Display(visible=0, size=(1600, 1024))
            display.start()
            self.driver = webdriver.Firefox()
            self.driver.delete_all_cookies()



        def doLogin(self,username,password):

            self.driver.get("https://www.linkedin.com/uas/login")
            self.driver.execute_script('localStorage.clear();')
            # Fixed UTF-8 issue in title.

            
            print ("[+] Login Page loaded successfully [+]")
            lnkUsername = self.driver.find_element_by_id("session_key-login")
            lnkUsername.send_keys(username)
            lnkPassword = self.driver.find_element_by_id("session_password-login")
            lnkPassword.send_keys(password)
            self.driver.find_element_by_id("btn-primary").click()
            if(str(self.driver.title) == "LinkedIn"):
                print ("[+] Login Success [+]")
                return True
            else:
                print ("[-] Login Failed [-]")
                return False



        def doGetLinkedin(self,url):
            self.driver.get(url)
            # Fix this with a better error Handling
            return self.driver.page_source.encode('ascii','replace')

        def doGetProfile(self,url):
            return Person(url, driver=self.driver)

        def getLinkedinLinks(self,state,company,pages_count=1):
            print ("[+] Getting profiles from Google [+]")
            dork = "site:%s.linkedin.com Current: %s" % (state , company)


            self.driver.get("https://www.google.com/search?q=%s&t=h_&ia=web" % dork)
            data = self.driver.page_source.encode('ascii','replace')
            if(pages_count > 1):
                for i in range(1,int(pages_count)):
                    start_at = 10 * i
                    print ("[+] Checking page %d on Google [+]" % i)
                    self.driver.get("https://www.google.com/search?q=%s&t=h_&ia=web&start=%d" % (dork,start_at))
                    data += self.driver.page_source.encode('ascii','replace')
            return data

        def kill(self):
            self.driver.quit()
    class Parser(object):
        company = ""
        linkedInUrl = ""
        htmlData = ""
        linkedinURLS = []
        def __init__(self,country):

            self.linkedInUrl = "https://%s.linkedin.com/in/" % country
            #self.company = company


        def readHTMLFile(self,htmlData):
            # This will initialize the html data
            self.htmlData = htmlData

        def getCompany():
            # Returns the company name
            return self.company

        def getExtractedLinks(self):
            # Returns Extracted Links
            if(self.htmlData == ""):
                exit()
            soupParser = BeautifulSoup(self.htmlData, 'html.parser')

            for link in soupParser.find_all('a'):
                
                temp = str(link.get('href'))
                
                if(temp.startswith(self.linkedInUrl)):
                    if(temp not in self.linkedinURLS):
                        self.linkedinURLS.append(temp)
                        
            return self.linkedinURLS
            # Return linkedinURLS array



        def getEmployeeInformation(self):
            # Will return the dictionary that contains employee data
            return


    # The response parameter is the data that every visited link will response basically the html page of the persons linkedin

        def extractName(self,response):
            # Will return the name and the surname from the response
            soupParser = BeautifulSoup(response, 'html.parser')
            name = soupParser.findAll("h1", class_="pv-top-card-section__name")[0].string

            return name

        def extractPosition(self,response):
            soupParser = BeautifulSoup(response, 'html.parser')
            position = soupParser.findAll("h2", class_="pv-top-card-section__headline")[0].string
            # To avoid big position names that will break the table format.
            if(len(position)>40):
                position = position[0:40]
            
            return position

        def extractCompany(self,response):
            # Will return the company from the response
            soupParser = BeautifulSoup(response, 'html.parser')
            company = soupParser.findAll("h3", class_="pv-top-card-section__company")[0].string
            return company
            


        def extractPhone(self,response):
            # Will return the phone if found
            # To be implemented
            return






    ParserObject = Parser(state)
    RequesterObject = Requester()



    pages_count = 1
    if pages is not None:
        pages_count = pages


    Persons = []

    # Download data from google search engine
    htmlData = RequesterObject.getLinkedinLinks(state,company,pages_count)


    # Parses the data from duck duck go
    ParserObject.readHTMLFile(htmlData)
    URLs = ParserObject.getExtractedLinks()


    # Will login the requester
    if(not RequesterObject.doLogin(linkedinUsername,linkedinPassword)):
        RequesterObject.kill()
        exit(0)

    for x in URLs:
        url = x.replace("https://%s." % state,"https://www.")
        emp=RequesterObject.doGetProfile(url)
        name = emp.name
        firstname =name.split()[0].lower()
        lastname =name.split()[1].lower()
        e1 = firstname + "@" + domain 
      
        #michaelombwayo@domain.com
        e2= firstname + lastname + "@" + domain 

        #michael.ombwayo@domain.com
        e3 = firstname + "." + lastname + "@" + domain

        #ombwayo@domain.com
        e4 = lastname + "@" + domain 

        #mombwayo@domain.com
        e5 = firstname[0] + lastname + "@" + domain 

        #m.ombwayo@domain.com
        e6= firstname[0] + "." + lastname + "@" + domain 

        #michaelo@domain.com
        e7 = firstname + lastname[0] + "@" + domain 

        #michael.o@domain.com
        e8 = firstname + "." + lastname[0] + "@" + domain 

        #mo@domain.com
        e9 = firstname[0] + lastname[0] + "@" + domain


      
        #michaelombwayo@domain.com
        e10= lastname + firstname + "@" + domain 

        #michael.ombwayo@domain.com
        e11 = lastname + "." + firstname + "@" + domain
        #mombwayo@domain.com
        e12 = lastname[0] + firstname + "@" + domain 

        #m.ombwayo@domain.com
        e13= lastname[0] + "." + firstname + "@" + domain 

        #michaelo@domain.com
        e14 = lastname + firstname[0] + "@" + domain 

        #michael.o@domain.com
        e15 = lastname + "." + firstname[0] + "@" + domain 

        #mo@domain.com
        e16 = lastname[0] + firstname[0] + "@" + domain

        emails =[e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12,e13,e14,e15,e16]
        target =Target.objects.get(target=domain)
        print("Generated Emails in relation to " +firstname +lastname)
        print("////////////////////////////")
        print("////////////////////////////")
        for email in emails:
            # try:
            records = dns.resolver.query(domain, 'MX')
            mxRecord = records[0].exchange
            mxRecord = str(mxRecord)

            #check if the email address Exist
            
            # Get local server hostname
            host = socket.gethostname()

            # SMTP lib setup (use debug level for full output)
            server = smtplib.SMTP()
            server.set_debuglevel(0)

            # SMTP Conversation
            server.connect(mxRecord)

            # SMTP Conversation
            server.connect(mxRecord)
            server.helo(host)
            server.mail('me@domain.com')
            code, message = server.rcpt(str(email))
            server.quit()

            # Assume 250 as Success
            if code == 250:
                retreived_employee =Employee.objects.update_or_create(company=target,name=emp.name,linked_url=url)
                try:
                    empd = Employee.objects.get(company=target,name=emp.name,linked_url=url)
                    Email_harvest_db.objects.update_or_create(employee=empd, email_records=email, target=target)
                except:
                    pass

                print(email + " " + " " + emp.name + " " + url)
            else:
                print(email + " " +' Does Not Exist')
            # except:
            #     print("Connection Refused.." +" "+ email + " " + "Cannot be verified")
            # print(emp.name + " " + url)


def ip_isp_checker(domain):
    ip_address = domaintoip(domain)
    db_ips = Service_provider.objects.all()
    ip = Service_provider.objects.filter(ip_adresses="41.206.32.3")
    return ip