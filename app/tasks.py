from django.utils.crypto import get_random_string
from celery import shared_task, current_task
import socket
import subprocess
from app.models import *
from ipwhois import IPWhois
from django.http import HttpResponse, HttpResponseRedirect, Http404
import os
from .scans import (domains_whois, ports,
    web_app_info, domain_dns_records,
    virtual_hosts, email_harvest, web_search, pagelinks,
    phone_numbers,vulnerabilities,nikto_vuln, generate_emails,link_in)
from celery_progress.backend import ProgressRecorder
def domaintoip(domain):
    return socket.gethostbyname(domain)

@shared_task
def create_random_user_accounts(search_text):
    total_user = 10
    for i in range (total_user):
        username = 'user_%s' % get_random_string(20, string.ascii_letters)
        email = '%s@example.com' % username
        password = get_random_string(50)
        User.objects.create_user(username=username, email=email, password=password)
        current_task.update_state(state='PROGRESS',
                                  meta={'current': i, 'total': total_user,
                                        'percent': int((float(i) / total_user) * 100)})
    return {'current': total_user, 'total': total_user, 'percent': 100}




@shared_task(bind=True)
def run_scans(self, search_text):
    progress_recorder = ProgressRecorder(self)
    
    ip_addr= domaintoip(search_text)
    domain = search_text
    progress_recorder.set_progress(0, 100)
    try:
        domains_whois(search_text, ip_addr)
    except:
        pass
    progress_recorder.set_progress(20, 100)
    try:
        ports(search_text,ip_addr)
    except:
        pass
    progress_recorder.set_progress(38, 100)
    
    vulnerabilities(search_text,ip_addr)
    
    progress_recorder.set_progress(43, 100)
  
    #nikto_vuln(search_text,ip_addr)

    progress_recorder.set_progress(59, 100)
    try:
        domain_dns_records(search_text,ip_addr)
    except:
        pass
    progress_recorder.set_progress(69, 100)
 
    virtual_hosts(search_text,ip_addr)
    progress_recorder.set_progress(78, 100)
    engine = 'google'
    email_harvest(domain, engine,ip_addr)
    progress_recorder.set_progress(89, 100)
    # pagelinks(search_text)
    progress_recorder.set_progress(92, 100)
    phone_numbers(search_text,ip_addr)
    progress_recorder.set_progress(96, 100)
   
    web_app_info(search_text,ip_addr)
    progress_recorder.set_progress(100, 100)
    return 'done'
    





@shared_task(bind=True)
def email_scans(self, search_text):
    target = Target.objects.get(id=search_text)
    domain_name=target.target
    employees = Employee.objects.filter(company=target)
    # for employee in employees:
    #     name=employee.name
    #     domain = str(employee.company.target)
    #     employee_id = employee.id
    link_in(domain_name)
    return "done"


@shared_task()
def automatic_scans():
    targets = Target.objects.all()
    for target in targets:
        search_text = target.domain
        try:
            ip_addr= domaintoip(search_text)
            domain = search_text
            domains_whois(search_text)
            ports(ip_addr, search_text)
            domain_dns_records(search_text)
            virtual_hosts(domain,search_text)
            engine = 'google'
            email_harvest(domain, engine)
        except:
            pass
        
    total_user = 10
    return {'current': total_user, 'total': total_user, 'percent': 100}





@shared_task()
def ping_sweeps():
    targets = Target.objects.all()
    for target in targets:
        try:
            ip_addr =domaintoip(target.target)
            proc = subprocess.Popen(
            ['ping', '-c', '3', ip_addr],
            stdout=subprocess.PIPE)
            stdout, stderr = proc.communicate()
            if proc.returncode == 0:
                status = "up"
                if target.status ==status and target.ip ==ip_addr:
                    pass
                else:
                    target.status=status
                    target.save()
                    TargetsPingTrack.objects.create(target=target,ip=ip_addr,status=status)


            else:
                status = "down"
                if target.status ==status and target.ip == ip_addr:
                    pass
                else:
                    target.status=status
                    target.save()
                    TargetsPingTrack.objects.create(target=target,ip=ip_addr,status=status)
        except:
            status= "down"
            if target.status==status:
                pass
            else:
                target.status =status
                target.save()
                TargetsPingTrack.objects.create(target=target,ip="Cannot be resolved",status=status)
        
    return "done"