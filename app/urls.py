from django.urls import  path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('scan/<int:pk>/', scan, name='scan'),
    path('targets_scan/', targets_scan, name='targets_scan'),
    path('compare_scans/<int:pk>/', compare_scans, name='compare_scans'),
    path('discard_scans/<int:pk>/', discard_scans, name='discard_scans'),
    path('vulnerabilities/<int:pk>/', vulnerabilities, name='vulnerabilities'),
    path('details/<int:pk>/', details, name='details'),
    path('<slug:slug>/employees/<int:pk>/', employees, name='employees'),
    path('employees_emails_scan/<int:pk>/', employees_emails_scan, name='employees_emails_scan'),
    path('update_target/<int:pk>/', update_target, name='update_target'),
    path('target_delete/<int:pk>/', target_delete, name='target_delete'),
    path('targets/', targets, name='targets'),
    path('vulnerability_scan/', vulnerability_scan, name='vulnerability_scan'),
    path('scan/',scans, name='scan'),
    path('<task_id>/', get_progress, name='task_status'),
    path('<task_id>/cancel_scan', cancel_scan, name='cancel_scan'),

   
    # path('add_target/',add_target, name='add_target'),


    # path('get-task-info/', get_task_info),
    # path('generate-user/', generate_random_user),

    #path('scantarget/', scantarget, name='scantarget'),


    
    

      ]