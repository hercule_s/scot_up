from django.shortcuts import render, get_object_or_404
import json
import socket
from app.models import *
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.urls import reverse
from app.tasks import run_scans, email_scans
from celery_progress.backend import Progress
from django.contrib.messages import get_messages
from celery.task.control import revoke
import os
import sys
import pymongo
from wig.wig import wig as WG
from bson import BSON
from bson import json_util
from pymongo import MongoClient
import re
from django.contrib import messages
from app.forms import TargetForm

import smtplib
from domain import domain_dnsrecords
from gsearch.googlesearch import search
from django.contrib.auth.decorators import login_required
import dns.resolver
from django.db.models import Q



def domaintoip(domain):
    return socket.gethostbyname(domain)





@login_required
def index(request):
    no ="no"
    context = {}
    form = TargetForm()
    if request.method == "POST" and "form1" in request.POST:
        d =request.POST['name']
        if Email_harvest_db.objects.filter(email_records = d).exists():
            domain_name = d.split('@')[1]
            check =Target.objects.filter(Q(ip=d)|Q(name__icontains=d)|Q(target=domain_name))
        else:
            check =Target.objects.filter(Q(ip=d)|Q(name__icontains=d)|Q(target=d))
        if check.count() >0:
            return render(request,'scot/index.html',{'form':form,'nbar': 'home','no':no, 'email':d,'targets':check,})
        else:
            if   re.match("[^@]+@[^@]+\.[^@]+", d):
                try:
                    domain_name = d.split('@')[1]
             
                    addressToVerify = d
                    
                    #Get MX Records
                    records = dns.resolver.query(domain_name, 'MX')
                    mxRecord = records[0].exchange
                    mxRecord = str(mxRecord)
                    # Get local server hostname
                    # Get local server hostname
                    host = socket.gethostname()

                    # SMTP lib setup (use debug level for full output)
                    server = smtplib.SMTP()
                    server.set_debuglevel(0)

                    # SMTP Conversation
                    server.connect(mxRecord)
                    server.helo(host)
                    server.mail('me@domain.com')
                    emailresults = search(d, num_results=10)
                    verified =""
                    # try:
                    code, message = server.rcpt(str(addressToVerify))
                    server.quit()

                    if code == 250:
                        verified = "Target Email Exists and is Valid"
                    else:
                        verified = "Email Verification Failed"
                except:
                    emailresults = search(d, num_results=10)
                    verified = "Email Server Refusing Connection.Verification Cannot be completed"
                return render(request,'scot/index.html',{'form':form,'nbar': 'home','no':no,'email':d,'domain':domain_name,'verified':verified, 'emailresults':emailresults})
            elif re.match(r"^(([a-z0-9]\-*[a-z0-9]*){1,63}\.?){1,255}$", d):
                results = search(d, num_results=6)
                try:
                    ip_addr =domaintoip(d)
                    response = os.system("ping -c 1 " + ip_addr)
                    if response == 0:
                        status = "up"
                    else:
                        status = "up"
                    return render(request,'scot/index.html',{'form':form,'nbar': 'home', 'domain':d, 'status':status, 'ip':ip_addr,'results':results})    
                except:
                    messages.add_message(request, messages.INFO, 'The Domain you entered is not linked to any IP Address')
                    return render(request,'scot/index.html',{'form':form,'nbar': 'home','no':no,'results':results,'domain':d,'message':messages})
                  
            else:
                serachresults = search(d, num_results=10)
                return render(request,'scot/index.html',{'form':form,'nbar': 'home','no':no, 'search':d, 'serachresults':serachresults})
   
    elif request.method == "POST" and "btnform2" in request.POST:
        form=TargetForm(request.POST)
        if form.is_valid():
            domain = form.cleaned_data.get('target')
            f = form.save(commit=False)
            try:
                ip_addr =domaintoip(domain)
                status ="up"
            except:
                ip_addr ="None"
                status ="down"
            f.ip = ip_addr
            f.status=status
            f.save()
            # t = Target.objects.create(target=domain, ip = ip_addr, status= status,name=name,country=country, sector=sector)#.objects.create(domain = d)
            s = TargetsPingTrack.objects.create(target=f,ip=f.ip,status=f.status)
            messages.add_message(request, messages.SUCCESS, 'Target Added Successfully')
            return HttpResponseRedirect('/app/targets/')

    elif request.method == "POST" and "btnform3" in request.POST:
       
        name = request.POST['name']
        email =request.POST['email']
        domain_name = email.split('@')[1]
        try:
            ip_addr =domaintoip(domain_name)
            response = os.system("ping -c 1 " + ip_addr)
            if response == 0:
                status = "up"
            else:
                status = "down"
        except:
            ip_addr = "None"
            status = "down"
        s = Target.objects.create(target=domain_name,ip=ip_addr,status=status,name=name)
        e = Email_harvest_db.objects.create(target=s,email_records=email)
        messages.add_message(request, messages.SUCCESS, 'Target Added Successfully')
        return HttpResponseRedirect('/app/targets/')

    else:
        return render(request,'scot/index.html',{'form':form,'nbar': 'home','no':no})

@login_required
def targets(request):
    # countries = Country.objects.all()
    # categories = Category.objects.all()
    # up= Target.objects.filter(status= "up").count()
    # down =Target.objects.filter(status= "Down").count()
    form = TargetForm()
    targets = Target.objects.all()
    if request.method == "POST" and "btnform2" in request.POST:
        form=TargetForm(request.POST)
        if form.is_valid():
            domain = form.cleaned_data.get('target')
            f = form.save(commit=False)
            try:
                ip_addr =domaintoip(domain_name)
            except:
                ip_addr = "None"
            try:
                response = os.system("ping -c 1 " + ip_addr)
                if response == 0:
                    status = "up"
                else:
                    status = "down"
            except:
                status = "down"
            f.ip = ip_addr
            f.status=status
            f.save()
            # t = Target.objects.create(target=domain, ip = ip_addr, status= status,name=name,country=country, sector=sector)#.objects.create(domain = d)
            s = TargetsPingTrack.objects.create(target=f,ip=f.ip,status=f.status)
            messages.add_message(request, messages.WARNING, 'Target Added Successfully')
            return HttpResponseRedirect('/app/targets/')
    return render(request,'scot/targets.html',{'targets':targets,'form':form})
@login_required
def update_target(request, pk):
    prof = get_object_or_404(Target, id=pk)
    if request.method == 'POST':
        form=TargetForm(request.POST or None, instance=prof)
        if form.is_valid():
            u=form.save(commit=False)
            u.save()
         
            messages.add_message(request, messages.SUCCESS, 'Target Updated Successfully')
            return HttpResponseRedirect('/app/targets')
    else:
        form = TargetForm(instance=prof)
    return render(request, 'scot/targetupdate.html',{'form':form,'target':prof})
def target_delete(request,pk):
    job=Target.objects.get(id=pk).delete()
    messages.add_message(request, messages.SUCCESS, 'Target Deleted successfully')
    return HttpResponseRedirect('/app/targets/')


@login_required
def targets_scan(request):
    countries = Country.objects.all()
    up= Target.objects.filter(status= "up").count()
    down =Target.objects.filter(status= "Down").count()
    return render(request,'scot/targets.html',{'countries':countries,'up':up,'down':down})



@login_required
def web_app_info(domain):
    app_information = WG(url=domain)
    fetch = app_information.run()
    results = app_information.get_results()
    list_results = dict(zip(results[::2], results[1::2]))
    # for r in list_results:
    print(list_results)
    # save_data = Web_app_info_db(app_info=str(results),target=domain)
    # save_data.save()
@login_required
def scan(request, pk):
    target = Target.objects.get(id=pk)
    search_text = target.target
    try:
        ip_addr= domaintoip(search_text)
    except:
        return HttpResponseRedirect('/app/targets/')
    task = run_scans.delay(search_text)
    targets = Target.objects.all()
    return render(request, 'scot/progress.html', context={'task_id': task.task_id,'targets':targets,'target':target})
    # url = reverse('details', kwargs={'slug': slug,'task_id': task.task_id})
    # return HttpResponseRedirect(url)
@login_required
def vulnerability_scan(request):
    targets = Target.objects

    # for target in  targets:
    #     try:
    #         ip_addr= domaintoip(target.target)
    #         web_app_info('ny')
            
    #     except:
    #         pass
    web_app_info('nyanzaonline.com')

    return HttpResponseRedirect('/app/targets/')

  
    return HttpResponseRedirect('/app/targets/')
    
@login_required
def details(request,pk):
    target =Target.objects.get(id=pk)
 
    # try:
    #     ip_addr = domaintoip(target.target)
    # except:
    #     return HttpResponseRedirect('/app/targets/')

    whois_data = Whois.objects.filter(target=target).order_by('-date')[:1]
    domain_dnsrecords = domain_dnsrecords_db.objects.filter(target=target).order_by('-date')[:1]
    hosts = Host.objects.filter(target=target)
    ports = Port.objects.filter(target=target)
    emails = Email_harvest_db.objects.filter(target=target)
    webs = Web_app_info_db.objects.filter(target=target)
    virtualhosts = Virtual_Host_db.objects.filter(target=target)
    vhosts = Virtual_Host.objects.filter(target=target)
    links = Pagelinks.objects.filter(target=target)
    phone_numbers = Phone_Number.objects.filter(target=target)
    tracks = TargetsPingTrack.objects.filter(target=target).order_by('date')
    # app_info_data = web_app_info(target.domain)
    #for d in app_info_data:
    # jsonToPython = json.loads(app_info_data)
    # print (jsonToPython )
   
    return render(request, 'scot/scan.html', {'phone_numbers':phone_numbers,'tracks':tracks,'target':target,'whois_data': whois_data,'vhosts':vhosts,'domain_dnsrecords':domain_dnsrecords,
    'hosts':hosts,'ports':ports,'emails':emails,'webs':webs, 'virtualhosts': virtualhosts, 'links': links})

@login_required    
def scans(request):

    return render(request, 'user.html', {'form': form})



def get_progress(request, task_id):
    progress = Progress(task_id)
    return HttpResponse(json.dumps(progress.get_info()), content_type='application/json')

@login_required
def cancel_scan(request, task_id):
    revoke(task_id, terminate=True)
    messages.add_message(request, messages.WARNING, 'Scan has been Successfully Canceled')
    return HttpResponseRedirect('/app/targets/')


# @login_required
# def compare_scans(request,pk):
#     target =Target.objects.get(id=pk)
#     whois_data = Whois.objects.filter(target=target).order_by('-date')[:1]
#     whois_staging = WhoisStaging.objects.filter(target=target).order_by('-date')[:1]
#     return render(request, 'scot/compare.html', {'target':target,'whois_data':whois_data,'whois_staging':whois_staging})

@login_required
def compare_scans(request,pk):
    target =Target.objects.get(id=pk)
    whois_data = Whois.objects.filter(target=target).order_by('-date')[:1]
    whois_staging = WhoisStaging.objects.filter(target=target).order_by('-date')[:1]
    domain_dnsrecords = domain_dnsrecords_db.objects.filter(target=target).order_by('-date')[:1]
    hosts = Host.objects.filter(target=target)
    ports = Port.objects.filter(target=target)
    emails = Email_harvest_db.objects.filter(target=target)
    webs = Web_app_info_db.objects.filter(target=target)
    virtualhosts = Virtual_Host_db.objects.filter(target=target)
    vhosts = Virtual_Host.objects.filter(target=target)
    links = Pagelinks.objects.filter(target=target)
    tracks = TargetsPingTrack.objects.filter(target=target).order_by('date')

    domain_dnsrecords_staging = domain_dnsrecords_db.objects.filter(target=target).order_by('-date')[:1]
    hosts_staging = Host.objects.filter(target=target)
    ports_staging = Port.objects.filter(target=target)
    emails_staging = Email_harvest_db.objects.filter(target=target)
    webs_staging = Web_app_info_db.objects.filter(target=target)
    virtualhosts_staging = Virtual_Host_db.objects.filter(target=target)
    vhosts_staging = Virtual_Host.objects.filter(target=target)
    links_staging = Pagelinks.objects.filter(target=target)
    return render(request, 'scot/compare.html', {'target':target,'whois_data': whois_data,'vhosts':vhosts,'domain_dnsrecords':domain_dnsrecords,
    'hosts':hosts,'ports':ports,'emails':emails,'webs':webs, 'virtualhosts': virtualhosts, 'links': links,
    'whois_staging': whois_staging,'vhosts_staging':vhosts_staging,'domain_dnsrecords_staging':domain_dnsrecords_staging,
    'hosts_staging':hosts_staging,'ports_staging':ports_staging,'emails_staging':emails_staging,'webs_staging':webs_staging, 'virtualhosts_staging': virtualhosts_staging, 'links_staging': links_staging})

@login_required
def compare_ports(request,pk):
    target =Target.objects.get(id=pk)
    whois_data = Whois.objects.filter(target=target).order_by('-date')[:1]
    whois_staging = WhoisStaging.objects.filter(target=target).order_by('-date')[:1]
    return render(request, 'scot/compare.html', {'target':target,'whois_data':whois_data,'whois_staging':whois_staging})
@login_required
def compare_emails(request,pk):
    target =Target.objects.get(id=pk)
    whois_data = Whois.objects.filter(target=target).order_by('-date')[:1]
    whois_staging = WhoisStaging.objects.filter(target=target).order_by('-date')[:1]
    return render(request, 'scot/compare.html', {'target':target,'whois_data':whois_data,'whois_staging':whois_staging})

@login_required
def compare_app_technologies(request,pk):
    target =Target.objects.get(id=pk)
    whois_data = Whois.objects.filter(target=target).order_by('-date')[:1]
    whois_staging = WhoisStaging.objects.filter(target=target).order_by('-date')[:1]
    return render(request, 'scot/compare.html', {'target':target,'whois_data':whois_data,'whois_staging':whois_staging})


@login_required
def discard_scans(request,pk):
    target =Target.objects.get(id=pk)
    whois_staging = WhoisStaging.objects.filter(target=target).delete()
   

    domain_dnsrecords_staging = domain_dnsrecords_db.objects.filter(target=target).delete()
    hosts_staging = Host.objects.filter(target=target).delete()
    ports_staging = Port.objects.filter(target=target).delete()
    emails_staging = Email_harvest_db.objects.filter(target=target).delete()
    webs_staging = Web_app_info_db.objects.filter(target=target).delete()
    virtualhosts_staging = Virtual_Host_db.objects.filter(target=target).delete()
    vhosts_staging = Virtual_Host.objects.filter(target=target).delete()
    links_staging = Pagelinks.objects.filter(target=target).delete()
    url = reverse('details', kwargs={'pk': target.id})
    return HttpResponseRedirect(url)

@login_required
def vulnerabilities(request, pk):
    target = Target.objects.get(id=pk)
    vulnerabilities = Vulnerabilities.objects.filter(target=target)
    sysvulnerabilities = SysVulnerabilities.objects.filter(target=target)
    return render(request, 'scot/vulnerabilities.html', {'form': 'form', 'sysvulnerabilities':sysvulnerabilities, 'vulnerabilities':vulnerabilities,})



@login_required
def employees(request, slug,pk):
    target = Target.objects.get(id=pk)
    employees = Employee.objects.filter(company=target)
    return render(request, 'scot/employees.html', {'target':target,'employees': employees,})


@login_required
def employees_emails_scan(request,pk):
    target = Target.objects.get(id=pk)
    search_text = target.id
    task = email_scans.delay(search_text)
    messages.add_message(request, messages.INFO, 'Employees Scan Started')
    url = reverse('employees', kwargs={'slug': target.slug,'pk': target.id})
    return HttpResponseRedirect(url)

