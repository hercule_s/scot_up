from django import template
import datetime
from app.models import Target

register =template.Library()
@register.inclusion_tag('emc/all.html')

def get_all_targets(event=None):
	return {'targets':Target.objects.all()}
