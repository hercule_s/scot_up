from django.urls import  path
from .views import *

urlpatterns = [
    
    path('', emc, name='emc'),
    path('search/', EMCTargetListView.as_view(), name='search'),
    path('target/<int:pk>/<slug:slug>/',EMCTargetDetailView.as_view(), name='target-emc'),
   
      ]