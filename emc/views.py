from django.shortcuts import render
from django.contrib.messages import get_messages
from django.contrib.auth.decorators import login_required
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from app.models import Target, Email_harvest_db
from django.db.models import Q
@login_required
def emc(request):
	return render(request, 'emc/emc.html', {})



class EMCTargetListView(LoginRequiredMixin,ListView):
	template_name ="emc/emc.html"
	model = Target
	def get_queryset(self, *args, **kwargs):
		qs = Target.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(
				Q(target__icontains=query) |
				Q(name__icontains=query))
		return qs
	context_object_name ='filtered_targets'


class EMCTargetDetailView(LoginRequiredMixin,DetailView):
	model =Target
	# context_object_name ='target'
	template_name ="emc/emc-target.html"
	def get_context_data(self, **kwargs):
		context = super(EMCTargetDetailView, self).get_context_data(**kwargs)
		context['emails'] = Email_harvest_db.objects.filter(target=self.get_object())
		return context



# class UserProfileDetailView(DetailView):
#     model = get_user_model()
#     slug_field = "username" 
#     template_name = "perfil.html"

#     def get_context_data(self, **kwargs):
#         # xxx will be available in the template as the related objects
#         context = super(UserProfileDetailView, self).get_context_data(**kwargs)
#         context['xxx'] = Email_harvest_db.objects.filter(target=self.get_object())
#         return context
