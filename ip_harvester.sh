#!/bin/bash

`curl --user-agent "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)" --silent http://bgp.he.net/country/KE | grep "/AS" > AS`

`cat AS | cut -f 4 -d '"' | grep -v AS327713 > Names`

`mkdir output`

while read -r line; do
	AS=`echo $line | cut -f 1 -d " "`
	NAME=`echo $line | cut -f 3- -d " "`

	`curl --user-agent "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)" --silent http://bgp.he.net/$AS#_prefixes > ASN`

	`cat ASN | grep -Eo '([0-9]{1,3}\.){3}[0-9]{1,3}\/([0-9]{2})' | uniq > subnets`
	`nmap -sL -n -iL subnets | grep 'Nmap scan report for' | cut -f 5 -d ' ' | uniq > output/"${NAME}"`

done < "Names"

exit 0