from bs4 import BeautifulSoup
import requests
from time import sleep
from selenium import webdriver
from pyvirtualdisplay import Display
import re


main_url = "http://bgp.he.net"
url = "http://bgp.he.net/country/DJ"
headers = {'User-Agent': 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0'}
driver = webdriver.Firefox()


def get_ASN_Name(url):
    result = requests.get(url, headers=headers)
    html= result.text
    cnt = result.content
    soup = BeautifulSoup(cnt, 'lxml')
    text = soup.get_text()
    names = soup.find("div", {"id": "country"}).findAll('tr')




    links = soup.find("div", {"id": "country"}).findAll('a', href=True)
    link_set = []
    for link in links:
        parsed_link = link.text
        link_set.append(link)

    link_final_set = []
    ASN_set = []
    for a in link_set:
        link_final = main_url + a['href']
        ASN_set.append(a['title'])
        link_final_set.append(link_final)

    #print(ASN_set)


    for as_link in link_final_set:
        # display = Display(visible=0, size=(1600, 1024))
        # display.start()
        driver.get(as_link)
        sleep(5)
    prefix_links = []
    for e in driver.find_elements_by_xpath('//table[@id="table_prefixes4"]//td[@class="nowrap"]//a'):
        ip_reslt = e.get_attribute('innerText')
        prefix_links.append(ip_reslt)

        print(prefix_links)

    ip_links = []
    for prefix in prefix_links:
        a = main_url + '/net/' +prefix
        ip_links.append(a)


    for prefix_link in ip_links:
        driver.get(prefix_link)
        sleep(5)

    ip_harvest = []
    for ip in driver.find_elements_by_xpath('//div[@id="dns"]//table//tbody//tr//a'):
        ip_raw = ip.get_attribute('innerText')
        ip_harvest.append(ip_raw)

    print(ip_harvest)
    IP_addresses = []
    for ip in ip_harvest:
        if len(ip) < 15:
            IP_addresses.append(ip)
        else:
            pass
    #print(IP_addresses)

    driver.close()
get_ASN_Name(url)
