
from tabulate import tabulate

import argparse
import sys
from selenium import webdriver
from pyvirtualdisplay import Display

from time import sleep

from bs4 import BeautifulSoup
import requests
from app.models import Employee

RED   = "\033[1;31m"
BLUE  = "\033[1;34m"

linkedinUsername = "kim34@tutanota.com"
linkedinPassword = "@#MkenyaDaima"
company = "'ICT Authority'"
state ="ke"
domain = "ict.go.ke"
pages =3

class Requester(object):

	timeout = 10

	def __init__(self):
		display = Display(visible=0, size=(1600, 1024))
		display.start()
		self.driver = webdriver.Firefox()
		self.driver.delete_all_cookies()



	def doLogin(self,username,password):

		self.driver.get("https://www.linkedin.com/uas/login")
		self.driver.execute_script('localStorage.clear();')
		# Fixed UTF-8 issue in title.

		
		print ("[+] Login Page loaded successfully [+]")
		lnkUsername = self.driver.find_element_by_id("session_key-login")
		lnkUsername.send_keys(username)
		lnkPassword = self.driver.find_element_by_id("session_password-login")
		lnkPassword.send_keys(password)
		self.driver.find_element_by_id("btn-primary").click()
		if(str(self.driver.title) == "LinkedIn"):
			print ("[+] Login Success [+]")
			return True
		else:
			print ("[-] Login Failed [-]")
			return False



	def doGetLinkedin(self,url):
		self.driver.get(url)
		# Fix this with a better error Handling
		return self.driver.page_source.encode('ascii','replace')

	def getLinkedinLinks(self,state,company,pages_count=1):
		print ("[+] Getting profiles from Google [+]")
		dork = "site:%s.linkedin.com Current: %s" % (state , company)


		self.driver.get("https://www.google.com/search?q=%s&t=h_&ia=web" % dork)
		data = self.driver.page_source.encode('ascii','replace')
		if(pages_count > 1):
			for i in range(1,int(pages_count)):
				start_at = 10 * i
				print ("[+] Checking page %d on Google [+]" % i)
				self.driver.get("https://www.google.com/search?q=%s&t=h_&ia=web&start=%d" % (dork,start_at))
				data += self.driver.page_source.encode('ascii','replace')
		return data

	def kill(self):
		self.driver.quit()
class Parser(object):
	company = ""
	linkedInUrl = ""
	htmlData = ""
	linkedinURLS = []
	def __init__(self,country):

		self.linkedInUrl = "https://%s.linkedin.com/in/" % country
		#self.company = company


	def readHTMLFile(self,htmlData):
		# This will initialize the html data
		self.htmlData = htmlData

	def getCompany():
		# Returns the company name
		return self.company

	def getExtractedLinks(self):
		# Returns Extracted Links
		if(self.htmlData == ""):
			exit()
		soupParser = BeautifulSoup(self.htmlData, 'html.parser')

		for link in soupParser.find_all('a'):
			
			temp = str(link.get('href'))
			
			if(temp.startswith(self.linkedInUrl)):
				if(temp not in self.linkedinURLS):
					self.linkedinURLS.append(temp)
					
		return self.linkedinURLS
		# Return linkedinURLS array



	def getEmployeeInformation(self):
		# Will return the dictionary that contains employee data
		return


# The response parameter is the data that every visited link will response basically the html page of the persons linkedin

	def extractName(self,response):
		# Will return the name and the surname from the response
		soupParser = BeautifulSoup(response, 'html.parser')
		name = soupParser.findAll("h1", class_="pv-top-card-section__name")[0].string

		return name

	def extractPosition(self,response):
		soupParser = BeautifulSoup(response, 'html.parser')
		position = soupParser.findAll("h2", class_="pv-top-card-section__headline")[0].string
		# To avoid big position names that will break the table format.
		if(len(position)>40):
			position = position[0:40]
		
		return position

	def extractCompany(self,response):
		# Will return the company from the response
		soupParser = BeautifulSoup(response, 'html.parser')
		company = soupParser.findAll("h3", class_="pv-top-card-section__company")[0].string
		return company
		


	def extractPhone(self,response):
		# Will return the phone if found
		# To be implemented
		return






ParserObject = Parser(state)
RequesterObject = Requester()



pages_count = 1
if pages is not None:
	pages_count = pages


Persons = []

# Download data from google search engine
htmlData = RequesterObject.getLinkedinLinks(state,company,pages_count)


# Parses the data from duck duck go
ParserObject.readHTMLFile(htmlData)
URLs = ParserObject.getExtractedLinks()


# Will login the requester
if(not RequesterObject.doLogin(linkedinUsername,linkedinPassword)):
    RequesterObject.kill()
    exit(0)

for x in URLs:
	url = x.replace("https://%s." % state,"https://www.")
	# person=Person("https://www.linkedin.com/in/ken-mwaura-56a63ab7", driver=driver)
	print(url)

