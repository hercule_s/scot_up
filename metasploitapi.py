import msgpack

import http.client

host = '192.168.1.230'

port = 55553

uri = '/api/'

headers = {'Content-type': 'binary / message-pack'}

client = http.client.HTTPConnection (host, port)

user = 'test'

password = 'test123456'

option = [user, password]

option.insert(0, 'auth.login')

params = msgpack.packb(option)

client.request('POST', uri, params, headers)

ret = client.getresponse()

response = msgpack.unpackb(ret.read())

print(response)
