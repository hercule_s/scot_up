import tensorflow as tf
import numpy as np
import tflearn
import unicodedata
import nltk
from nltk.stem.lancaster import LancasterStemmer
import sys
import json
import random

tbl = dict.fromkeys(i for i in range(sys.maxunicode)
                    if unicodedata.category(chr(i)).startswith('P'))

def remove_punctuation(text):
    return text.translate(tbl)

stemmer = LancasterStemmer()
data = None

with open('data.json') as json_data:
    data = json.load(json_data)
    print(data)

categories = list(data.keys())
words = []
docs = []

for each_category in data.keys():
    for each_sentence in data[each_category]:
        each_sentence = remove_punctuation(each_sentence)
        print(each_sentence)
        w = nltk.word_tokenize(each_sentence)
        print("tokenized words: ", w)
        words.extend(w)
        docs.append((w, each_category))


words = [stemmer.stem(w.lower()) for w in words]
words = sorted(list(set(words)))

training = []
output = []
output_empty = [0] * len(categories)


for doc in docs:
    bow = []
    token_words = doc[0]
    token_words = [stemmer.stem(word.lower()) for word in token_words]
    for w in words:
        bow.append(1) if w in token_words else bow.append(0)

    output_row = list(output_empty)
    output_row[categories.index(doc[1])] = 1
    training.append([bow, output_row])

random.shuffle(training)
training = np.array(training)

train_x = list(training[:, 0])
train_y = list(training[:, 1])

tf.reset_default_graph()

net = tflearn.input_data(shape=[None, len(train_x[0])])
net = tflearn.fully_connected(net, 8)
net = tflearn.fully_connected(net, 8)
net = tflearn.fully_connected(net, len(train_y[0]), activation='softmax')
net = tflearn.regression(net)

model = tflearn.DNN(net, tensorboard_dir='tflearn_logs')
model.fit(train_x, train_y, n_epoch=10000, batch_size=8, show_metric=True)
model.save('model.tflearn')

scan_1 = '21, 80, 443'
scan_2 = 'http, https, pop3, ssh'
scan_3 = '1.2.3, 4.0.1, 2.1.3'
scan_4 = 'OpenSSH, Apache httpd'

def get_tf_record(sentence):
    global words
    sentence_words = nltk.word_tokenize(sentence)
    sentence_words = [stemmer.stem(word.lower()) for word in sentence_words]
    bow = [0]*len(words)
    for s in sentence_words:
        for i, w in enumerate(words):
            if w == s:
                bow[i] = 1

    return(np.array(bow))


print(categories[np.argmax(model.predict([get_tf_record(scan_1)]))])
print(categories[np.argmax(model.predict([get_tf_record(scan_2)]))])
print(categories[np.argmax(model.predict([get_tf_record(scan_3)]))])
print(categories[np.argmax(model.predict([get_tf_record(scan_4)]))])