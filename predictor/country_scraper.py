import requests
from bs4 import BeautifulSoup

url = "http://www.education.go.ug/data/smenu/48/Ministry Location .html"
response = requests.get(url)
html = response.text

soup = BeautifulSoup(html, "html5lib")


#page title
#print(soup.title.string)

#urls in the page
soup.findAll('a')
text = soup.get_text()
# print(text)

#tokenize
import re
ps = '\w+'
tokens = re.findall(ps, text)
#print(tokens)

#make lowercase

words = []
for word in tokens:
    words.append(word.lower())

#common english stop words
import nltk
sw = nltk.corpus.stopwords.words('english')


#important words
words_ns = []

for word in words:
    if word not in sw:
        words_ns.append(word)



import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

# Figures inline and set visualization style
sns.set()

# Create freq dist and plot
freqdist1 = nltk.FreqDist(words_ns)
freqdist1.plot(25)
