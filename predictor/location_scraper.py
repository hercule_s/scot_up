from bs4 import BeautifulSoup
import requests

url = "http://www.education.go.ug/data/smenu/48/Ministry%20Location%20.html"

def get_location(url):
    result = requests.get(url)
    html = result.text
    cnt = result.content
    soup = BeautifulSoup(cnt, 'lxml')
    text = soup.get_text()
    locations = soup.find("div", {"id": "right_panel"}).findAll('p')
    for loc in locations:
        print(loc.text)

get_location(url)