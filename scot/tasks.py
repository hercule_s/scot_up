from __future__ import absolute_import, unicode_literals
import string
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from celery import shared_task, current_task

@shared_task
def create_random_user_accounts(total_user):
    for i in range(total_user):
        username = 'user_%s' % get_random_string(20, string.ascii_letters)
        email = '%s@example.com' % username
        password = get_random_string(50)
        User.objects.create_user(username=username, email=email, password=password)
        current_task.update_state(state='PROGRESS',
                                  meta={'current': i, 'total': total_user,
                                        'percent': int((float(i) / total_user) * 100)})
    return {'current': total_user, 'total': total_user, 'percent': 100}

# import string

# from django.contrib.auth.models import User
# from django.utils.crypto import get_random_string

# from celery import shared_task,current_task
# from numpy import random
# from scipy.fftpack import fft
# from celery_progress.backend import ProgressRecorder
# import time
# @shared_task(bind=True)
# def create_random_user_accounts(self,total):
# 	progress_recorder = ProgressRecorder(self)
# 	for i in range(total):
# 	    username = 'user_{}'.format(get_random_string(10, string.ascii_letters))
# 	    email = '{}@example.com'.format(username)
# 	    password = get_random_string(50)
# 	    User.objects.create_user(username=username, email=email, password=password)
# 	    progress_recorder.set_progress(i + 1, seconds)
# 	return '{} random users created with success!'.format(total)




# @shared_task
# def fft_random(n):
#     """
#     Brainless number crunching just to have a substantial task:
#     """
#     for i in range(n):
#         x = random.normal(0, 0.1, 2000)
#         y = fft(x)
#         if(i%30 == 0):
#             process_percent = int(100 * float(i) / float(n))
#             current_task.update_state(state='PROGRESS',
#                                       meta={'process_percent': process_percent})
#     return random.random()

# @shared_task
# def add(x,y):
#     for i in range(1000000000):
#         a = x+y
#     return x+y
# from celery import task, current_task
# from celery.result import AsyncResult
# from time import sleep
# from app.models import *


# NUM_OBJ_TO_CREATE = 1000

# @task()
# def create_models():
# 	for i in range(1, NUM_OBJ_TO_CREATE+1):
# 		fn = 'Fn %s' % i
# 		ln = 'Ln %s' % i
# 		my_model = models.MyModel(fn=fn, ln=ln)
# 		my_model.save()
		
# 		process_percent = int(100 * float(i) / float(NUM_OBJ_TO_CREATE))

# 		sleep(0.1)
# 		current_task.update_state(state='PROGRESS',
# 				meta={'process_percent': process_percent})