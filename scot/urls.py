"""scot URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings
from django.conf.urls.static import static
from app.views import index
from allauth.account import views as allauthviews
urlpatterns = [
    re_path(r'^$', allauthviews.login),
    path('scot/kiongozi/', admin.site.urls),
    path('app/', include('app.urls')),
    path('emc/', include('emc.urls')),
    path('social/', include('socialengineering.urls')),
    path('weapons/', include('weapon.urls')),
    path('celery-progress/', include('celery_progress.urls')),
    path('accounts/', include('allauth.urls')),
    path('webpush/', include('webpush.urls')),
    path('api-auth/', include('rest_framework.urls')),
] +static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
