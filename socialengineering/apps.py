from django.apps import AppConfig


class SocialengineeringConfig(AppConfig):
    name = 'socialengineering'
