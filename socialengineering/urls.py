from django.urls import  path
from .views import *

urlpatterns = [
    path('', EmailsListView.as_view(), name='socialemails'),
    path('campaigns', campaigns, name='campaigns'),
    path('groups', GroupListView.as_view(), name='groups'),
    path('templates', EmailTemplatesListView.as_view(), name='templates'),
    path('landing_pages', LandingPagesListView.as_view(), name='landing_pages'),
    path('sending_profiles', SendingProfilesListView.as_view(), name='sending_profiles'),
    
      ]