from django.shortcuts import render
import requests
from django.contrib.messages import get_messages
from django.contrib.auth.decorators import login_required
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from app.models import Target, Email_harvest_db
from django.db.models import Q
from gophish import Gophish
api_key = 'f4706bb38e662eba2a30e3f23197ad00cf6fe8b2895f703a01d3b43523376c26'
headers = { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0' }
api = Gophish(api_key,host=' https://127.0.0.1:9000',headers=headers, verify=False)

class EmailsListView(LoginRequiredMixin,ListView):
	template_name ="social/emails.html"
	model = Email_harvest_db
	def get_queryset(self, *args, **kwargs):
		qs = Email_harvest_db.objects.all()
		return qs
	context_object_name ='emails'




def campaigns(request):
	
	data = api.campaigns.get()
	return render(request, 'social/camp.html', {'data': data})


class GroupListView(LoginRequiredMixin,ListView):
	template_name ="social/groups.html"
	def get_queryset(self, *args, **kwargs):
		qs =  api.groups.get()
		return qs
	context_object_name ='groups'



class EmailTemplatesListView(LoginRequiredMixin,ListView):
	template_name ="social/templates.html"
	def get_queryset(self, *args, **kwargs):
		qs =  api.templates.get()
		return qs
	context_object_name ='templates'


class LandingPagesListView(LoginRequiredMixin,ListView):
	template_name ="social/pages.html"
	def get_queryset(self, *args, **kwargs):
		qs =  api.pages.get()
		return qs
	context_object_name ='pages'

class SendingProfilesListView(LoginRequiredMixin,ListView):
	template_name ="social/profiles.html"
	def get_queryset(self, *args, **kwargs):
		qs =  api.smtp.get()
		return qs
	context_object_name ='profiles'

