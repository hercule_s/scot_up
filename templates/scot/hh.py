import socket
from app.models import *
from ipwhois import IPWhois
import nmap
from wig.wig import wig as WG
from botanick import Botanick as bt
import dns.resolver
import subprocess
import shlex
import robtex_python as rob
from domain import domain_dnsrecords
from extract_emails import ExtractEmails
import urllib.parse
import urllib
import urllib.request
import json
import pythonwhois
import whois
from bs4 import BeautifulSoup
import requests
from builtwith import builtwith
from pymongo import MongoClient


def domaintoip(domain):
    return socket.gethostbyname(domain)

def domains_whois(search_text):
    target = Target.objects.get(target=search_text)
    ip_addr = domaintoip(search_text)
    data = IPWhois(ip_addr)
    out2 = data.lookup_rdap(depth=1)
    out = data.lookup_whois(get_referral=True)
    results = pythonwhois.get_whois(search_text)
    whois_results = whois.whois(search_text)

    asn = out2["asn"]
    asn_cidr = out2["asn_cidr"]
    asn_registry =out2["asn_registry"]
    status = out2["network"]["status"]



    city_data = out["nets"][0]['city']
    country_data = out["nets"][0]['country']
    description_data = out["nets"][0]['description']
    emails_data = out["nets"][0]['emails']
    name_data = out["nets"][0]['name']
    range_data = out["nets"][0]['range']
    state_data = out["nets"][0]['state']
    

    registrar_data = whois_results.registrar
    server_info = whois_results.whois_server
    domain_owner = whois_results.name
    owner_city = whois_results.city
    owner_state = whois_results.state
    owner_country = whois_results.country
    owner_company = whois_results.org
    domain_emails = whois_results.emails
    try:
        whois = Whois.ogjects.get(target=target)
        WhoisStaging.objects.filter(target=target).delete()
        WhoisStaging.objects.create(target=target,ip=str(ip_addr), emails=str(emails_data),sh_domain=str(search_text),  city=str(city_data), country=str(country_data),
                     description=str(description_data),  name=str(name_data),
                     ip_range=str(range_data), registrar=registrar_data,server_info=server_info, 
                     owner=domain_owner,owner_city=owner_city, owner_state=owner_state,
                     owner_country=owner_country, owner_company=owner_company,
                     domain_emails=domain_emails, state=str(state_data),asn =str(asn),asn_cidr = str(asn_cidr),
                     asn_registry =str(asn_registry),status = str(status))
    except:
        Whois.objects.create(target=target,ip=str(ip_addr), emails=str(emails_data),sh_domain=str(search_text),  city=str(city_data), country=str(country_data),
                         description=str(description_data),  name=str(name_data),
                         ip_range=str(range_data), registrar=registrar_data,server_info=server_info, 
                         owner=domain_owner,owner_city=owner_city, owner_state=owner_state,
                         owner_country=owner_country, owner_company=owner_company,
                         domain_emails=domain_emails, state=str(state_data),asn =str(asn),asn_cidr = str(asn_cidr),
                         asn_registry =str(asn_registry),status = str(status))

    # Whois.objects.update_or_create(ip=(ip_addr), sh_domain=(search_text), target=search_text, city=(city_data), country=(country_data),
    #                  description=(description_data), name=(name_data),
    #                  range=(range_data), state=(state_data))





def ports(domain, search_text):
    target = Target.objects.get(target=search_text)
    ip_addr= domaintoip(domain)
    nm = nmap.PortScanner()
    nm.scan(ip_addr,'21-30')
    for host in nm.all_hosts():
        status= nm[host].state()
        hs = str(host)
        try:
            Host.objects.get(target=target,host=hs)
            HostStaging.objects.filter(target=target,host=hs).delete()
            HostStaging.objects.create(target=target,host=hs,status=status)
        except:
            Host.objects.create(target=target,host=hs,status=status)
            Target.objects.filter(target =search_text).update(status=status)
        for proto in nm[host].all_protocols():
            lport = nm[host][proto].keys()
            for port in lport:
                try:
                    Port.objects.get(target=target,
                    port =str(port))
                    PortStaging.objects.filter(target=target,
                    port =str(port)).delete()
                    PortStging.objects.create(target=target,
                    port =str(port), status=str(nm[host][proto][port]['state']))
                except:
                    Port.objects.create(target=target,
                    port =str(port), status=str(nm[host][proto][port]['state']))

def email_harvest(search_text, engine):
    bt.callEmailHarvester(search_text, engine)
    email_records = bt.getResults()
    parsed = urllib.parse.urlparse(search_text)
    target = Target.objects.get(target=search_text)
    if parsed.scheme == '':
        try:
            replaced_parse = parsed._replace(scheme='http')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            extract_from_domain = ExtractEmails(url, depth=None, print_log=False, ssl_verify=True, user_agent='random')
            email_records2 = extract_from_domain.emails
            Email_harvest_db.objects.update_or_create( target=target,email_records=str(email_records),
                )
            
            for email in email_records2:
                try:
                    Email_harvest_db.objects.get(target=target,
                    email_records =str(email))
                    Email_harvest_db_staging.objects.filter(target=target,
                    email_records =str(email)).delete()
                    Email_harvest_db_staging.objects.create(target=target,
                    email_records =str(email))
                except:
                    Email_harvest_db.objects.update_or_create(target=target,
                    email_records =str(email))
        except:
            replaced_parse = parsed._replace(scheme='https')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            extract_from_domain = ExtractEmails(url, depth=None, print_log=False, ssl_verify=True, user_agent='random')
            email_records2 = extract_from_domain.emails
            Email_harvest_db.objects.update_or_create( target=target,email_records=str(email_records))
            
            for email in email_records2:
                try:
                    Email_harvest_db.objects.get(target=target,
                    email_records =str(email))
                    Email_harvest_db.objects_staging.update_or_create(target=target,
                    email_records =str(email))
                except:
                    Email_harvest_db.objects.update_or_create(target=target,
                    email_records =str(email))
            

def domain_dns_records(domain):
    target = Target.objects.get(target=domain)
    soa_records = domain_dnsrecords.fetch_dns_records(domain, 'SOA')
    soa_out = ("".join(map(str, soa_records)))
    mx_records = domain_dnsrecords.fetch_dns_records(domain, 'MX')
    mx_out = ("".join(map(str, mx_records)))
    txt_records = domain_dnsrecords.fetch_dns_records(domain, 'TXT')
    txt_out = ("".join(map(str, txt_records)))
    a_records = domain_dnsrecords.fetch_dns_records(domain, 'A')
    a_out = ("".join(map(str, a_records)))
    name_server_records = domain_dnsrecords.fetch_dns_records(domain, 'NS')
    name_out = ("".join(map(str, name_server_records)))
    cname_records = domain_dnsrecords.fetch_dns_records(domain, 'CNAME')
    cname_out = ("".join(map(str, cname_records)))
    aaaa_records = domain_dnsrecords.fetch_dns_records(domain, 'AAAA')
    print (aaaa_records)
    aaaa_out = ("".join(map(str, aaaa_records)))
    try:
        domain_dnsrecords_db.objects.get(target=target)
        domain_dnsrecords_db_staging.objects.update_or_create(target=target,soa_records=str(soa_out),
         aaaa_records=str(aaaa_out),cname_records=str(cname_out),
           a_records=str(a_out),txt_records=str(txt_out),mx_records=str(mx_out))
    except:
        domain_dnsrecords_db.objects.create(target=target,soa_records=str(soa_out),
         aaaa_records=str(aaaa_out),cname_records=str(cname_out),
           a_records=str(a_out),txt_records=str(txt_out),mx_records=str(mx_out))
   

def virtual_hosts(domain, search_text):
    ip_addr = domaintoip(domain)
    response = rob.ip_query(ip_addr)
    country = response['country']
    autonomous_system_name = response['asname']
    bgp_route = response['bgproute']
    # try:
    target = Target.objects.get(target=search_text)
    passive_domains = response['pas']
    for d in passive_domains:
        v = d['o']
        try:
            Virtual_Host.objects.get(target=target,v_hosts=str(v))
            Virtual_Host_Staging.objects.update_or_create(target=target,v_hosts=str(v))
        except:
            Virtual_Host.objects.update_or_create(target=target,v_hosts=str(v))
  
    inactive_domains = response['pash']
    for d in inactive_domains:
        v = d['o']
    
        try:
            Virtual_Host.objects.get(target=target,v_hosts=str(v))
            Virtual_Host_Staging.objects.update_or_create(target=target,v_hosts=str(v))
        except:
            Virtual_Host.objects.update_or_create(target=target,v_hosts=str(v))
  
    try:
        main_domain = response['act']
        for d in main_domain:
            v = d['o']
            try:
                Virtual_Host_db.objects.get(target=target,main_domain=str(v),
                 location=str(country),asname=str(autonomous_system_name),
                  bgp_route=str(bgp_route))
                Virtual_Host_db_staging.objects.update_or_create(target=target,main_domain=str(v),
                 location=str(country),asname=str(autonomous_system_name),
                  bgp_route=str(bgp_route))
            except:
                Virtual_Host_db.objects.create(target=target,main_domain=str(v),
                 location=str(country),asname=str(autonomous_system_name),
                  bgp_route=str(bgp_route))
    except:
        try:
            Virtual_Host_db.objects.get(main_domain=search_text, target=target,
             location=str(country),asname=str(autonomous_system_name),
              bgp_route=str(bgp_route))
            Virtual_Host_db_staging.objects.update_or_create(main_domain=search_text, target=target,
             location=str(country),asname=str(autonomous_system_name),
              bgp_route=str(bgp_route))
        except:
            Virtual_Host_db.objects.create(main_domain=search_text, target=target,
             location=str(country),asname=str(autonomous_system_name),
              bgp_route=str(bgp_route))
               



def pagelinks_domain(domain, search_text):
    target = Target.objects.get(target=search_text)
    pagelinks_records = domain_pagelinks.pagelinks(domain)
    pagelinks_records = set(pagelinks_records)
    for x in pagelinks_records:
        try:
            domain_pagelinks_db.objects.get(domain_pagelink=x, target=target)
            domain_pagelinks_db_staging.objects.update_or_create(domain_pagelink=x, target=target)
        except:
            save_data = domain_pagelinks_db.objects.update_or_create(domain_pagelink=x, target=target)


def web_search(search_text):
    # text = str(search_text + ":")
    # query = raw_input("search_text:")
    target = Target.objects.get(target=search_text)
    url = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&"
    query = urllib.parse.urlencode( {'q' : search_text } )
    response = urllib.request.urlopen (url + query ).read()
    data = json.loads ( response.decode() )
    results = data[ 'responseData' ][ 'results' ]
    for res in results:
        title=res.title.encode("utf8")
        desc=res.desc.encode("utf8")
        url =res.url.encode("utf8")
        Web_search.objects.create(target=target,
        desc=str(desc),url=str(url))


def pagelinks(domain):
    target = Target.objects.get(target=domain)
    parsed = urllib.parse.urlparse(domain)
    if parsed.scheme == '':
        try:
            replaced_parse = parsed._replace(scheme='http')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            soup = BeautifulSoup(requests.get(url).text, 'lxml')

            hrefs = []

            for a in soup.find_all('a'):
                hrefs.append(a['href'])

            for link in hrefs:
                try:
                    Pagelinks.objects.get(target=target,
                    links=link)
                    PagelinksStaging.objects.update_or_create(target=target,
                    links=link)
                except:
                    Pagelinks.objects.update_or_create(target=target,
                    links=link)



        except:
            replaced_parse = parsed._replace(scheme='https')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            soup = BeautifulSoup(requests.get(url).text, 'lxml')

            hrefs = []

            for a in soup.find_all('a'):
                hrefs.append(a['href'])

            for link in hrefs:
                try:
                    Pagelinks.objects.get(target=target,
                    links=link)
                    PagelinksStaging.objects.update_or_create(target=target,
                    links=link)
                except:
                    Pagelinks.objects.update_or_create(target=target,
                    links=link)



def web_app_info(search_text):
    target = Target.objects.get(target=search_text)
    parsed = urllib.parse.urlparse(search_text)
   
    if parsed.scheme == '':
        try:
            replaced_parse = parsed._replace(scheme='http')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            app_information = builtwith(url)
            for key, value in app_information.items():
                try:
                    Web_app_info_db.objects.get(target=target,
                    key=str(key), app_info="".join([str(x) for x in value] ))
                    Web_app_info_db_staging.objects.update_or_create(target=target,
                   key=str(key), app_info="".join([str(x) for x in value] ))
                except:
                    Web_app_info_db.objects.update_or_create(target=target,
                    key=str(key), app_info="".join([str(x) for x in value] ))

        except:
            replaced_parse = parsed._replace(scheme='https')
            url = "{0.scheme}://{0.path}".format(replaced_parse)
            app_information = builtwith(url)
            for key, value in app_information.items():
                try:
                    Web_app_info_db.objects.get(target=target,
                    key=str(key), app_info="".join([str(x) for x in value] ))
                    Web_app_info_db_staging.objects.update_or_create(target=target,
                   key=str(key), app_info="".join([str(x) for x in value] ))
                except:
                    Web_app_info_db.objects.update_or_create(target=target,
                    key=str(key), app_info="".join([str(x) for x in value] ))

            # Web_app_info_db.objects.filter(target=search_text, app_info=app_information).update_one(
            #     target=str(search_text),
            #     app_info=str(app_information), upsert=True)
